# Python Docker CI Template with Crontab

Python Docker CI with Crontab allows creation of a docker container with python code that runs on a cron schedule.

## Cron schedule
The cron schedule is set in cron-setup.sh, which sets up the schedule when the container is started.  Set the schedule here by changing the crontab.
```console
...
# The default schedule is 30 minutes past the hour from Monday to Friday
# Please note the time zone!
echo "30 * * * 1-5 
...
```

## Environment Variables
Environment variables for the docker container will not be available to the cron job, instead, they must be passed explicitly in the **cron-setup.sh** file.
```console
...
# Set the variables in cron-setup.sh
echo "30 * * * 1-5 export $VARIABLE1 = "somevariable"
...
```

## Main Program
Set the python program which will run at the given schedule in the **task.sh** file.
```console
...
# Call you Python Entry Point Here
python3 /code/hello.py
...
```

## Python Dependancies
Add any depedancies needed by the python program to the **requirements.txt** file.  These will be setup when the container is built.  To avoid breaking features specifiy a version when possible
```console
...
# It is wise to specify a version for compatibility
numpy == 1.21.2
...
```

## Logs
Python logs get sent to the standard output.  These can be used with the docker logs function.
```console
sudo docker logs container-name
```

## Timezone
The timezone can be set in the **DOCKERFILE** or by overriding the timezone environment variable when building the Dockerfile.
```console
# ***********************
# Build 2: Set the timezone
#
ENV TZ America/Toronto
```
