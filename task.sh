#!/bin/bash
PATH=/usr/local/bin:$PATH
echo " *** $(date)  Running the cron schedule ***"

# Call you Python Entry Point Here
python3 /code/hello.py

echo " *** $(date)  Completed the cron schedule ***"
