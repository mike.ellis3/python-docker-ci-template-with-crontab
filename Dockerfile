# Version specific for compatibility
FROM python:3.9.7-slim-bullseye

# set the working directory in the container
WORKDIR /code

# ***********************
# Build 1: Install the Debian Environment Dependencies
# 
RUN apt-get update && apt-get install -y \
  tzdata \
  cron \
  && rm -rf /var/lib/apt/lists/*

# ***********************
# Build 2: Set the timezone
#
ENV TZ America/Toronto

# ***********************
# Build 3: Install the Python Depedencies
# 
COPY requirements.txt .
RUN pip install -r requirements.txt

# ***********************
# Build 4: Setup the Cron Schedule
#
COPY task.sh /usr/local/bin/task.sh
COPY cron-setup.sh /usr/local/bin/cron-setup.sh
RUN chmod +x /usr/local/bin/task.sh &&\
    chmod +x /usr/local/bin/cron-setup.sh

# ***********************
# Build 5: Copy the Source Code into the container
#
COPY src/ .

# Entrypoint keyword is requried to run cron
ENTRYPOINT ["/usr/local/bin/cron-setup.sh"]
