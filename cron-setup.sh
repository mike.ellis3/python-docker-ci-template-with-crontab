#!/bin/sh

# Start the run once job.
echo "Docker container has been started"

# One time environment setup here

# Setup a cron schedule
# Variables need to be passed
echo "30 * * * 1-5 export VARIABLE1="hello"; \
                     export VARIABLE2="world"; \
                     /usr/local/bin/task.sh >> /var/log/task.log 2>&1
# This extra line makes it a valid cron" > scheduler.txt
crontab scheduler.txt
cron -f
